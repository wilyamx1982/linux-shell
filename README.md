# Linux Shell Commands

1. Printing the current working directory

		$ pwd
		/WILLIAM/_GIT/ThinkItTwice/cms
		
2. Environment variables

		$ printenv
		$ unset env-variable

3. Print host machine specs

		$ uname -a
		Linux ip-10-0-1-80 4.4.0-1087-aws #98-Ubuntu SMP Wed Jun 26 05:50:53 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux

4. How to install Vim from Remote Server

		$ apt-get update
		$ apt-get install vim
		
## Directories	

1. Common paths for executable files `/usr/local/bin`

	

